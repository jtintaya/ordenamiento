// MÉTODO DE BURBUJA
const burbuja = (arrB) => {
  const l = arrB.length;
  const startTime = moment().milliseconds();
  let endTime = null;
  for (let i = 0; i < l; i++) {
    for (let j = 0; j < l - 1 - i; j++) {
      if (arrB[j] > arrB[j + 1]) {
        [arrB[j], arrB[j + 1]] = [arrB[j + 1], arrB[j]];
      }
    }
  }
  endTime = moment().milliseconds();
  //console.log("Inicio: ", startTime, "Fin: ", endTime, "Diferencia: ", (endTime-startTime) );
  return {
    data: arrB,
    time: endTime - startTime,
  };
};
// Tamaño de longitud de array
let tamArray = Math.floor(Math.random() * 1500);
let arrB = [];
for (let i = 0; i < tamArray; i++) {
  arrB.push(Math.floor(Math.random() * 500));
}

let resBubble = burbuja(arrB);
console.log(resBubble.data, resBubble.time);
document.getElementById(
  "p1"
).innerHTML = `Buble Sort: ${resBubble.time} milesegundos`;
resBubble = resBubble.data;

// MÉTODO DE INSERCIÓN
const insercion = (arrI) => {
  const l = arrI.length;
  let j, temp;
  const startTime = moment().milliseconds();
  let endTime = null;
  for (let i = 1; i < l; i++) {
    j = i;
    temp = arrI[i];
    while (j > 0 && arrI[j - 1] > temp) {
      arrI[j] = arrI[j - 1];
      j--;
    }
    arrI[j] = temp;
  }

  //return arrI;
  endTime = moment().milliseconds();
  return {
    data: arrI,
    time: endTime - startTime,
  };
};

const arrI = arrB;
const resInsertion = insercion(arrI);
console.log(resInsertion.data, resInsertion.time);
document.getElementById(
  "p2"
).innerHTML = `Insertion Sort: ${resInsertion.time} milesegundos`;

resInsertion;

// MÉTODO DE SELECCIÓN
const seleccion = (arrS) => {
  const startTime = moment().milliseconds();
  let endTime = null;
  for (let j = 0; j < arrS.length; ++j) {
    let i = (iMin = j);
    for (++i; i < arrS.length; ++i) {
      arrS[i] < arrS[iMin] && (iMin = i);
    }
    [arrS[j], arrS[iMin]] = [arrS[iMin], arrS[j]];
  }

  // return arrS;
  endTime = moment().milliseconds();
  return {
    data: arrS,
    time: endTime - startTime,
  };
};

const arrS = arrB;
const resSelection = seleccion(arrS);
console.log(resSelection.data, resSelection.time);
document.getElementById(
  "p3"
).innerHTML = `Selection Sort: ${resSelection.time} milesegundos`;
console.log(resSelection);
resSelection;

// MÉTODO DE MEZCLA
const mezcla = (arrM) => {
  
  let endTime = null;
  if (arrM.length < 2) {
    return arrM;
  }

  const middle = parseInt(arrM.length / 2) | 0;
  const left = arrM.slice(0, middle);
  const right = arrM.slice(middle);
  const merge = (left, right) => {
    const resMerge = [];
    let il = (ir = 0);

    while (il < left.length && ir < right.length) {
      resMerge.push(left[il] < right[ir] ? left[il++] : right[ir++]);
    }

    return [...resMerge, ...left.slice(il), ...right.slice(ir)];
  };

  return merge(mezcla(left), mezcla(right));
};

const arrM = arrB;
const startTime = moment().milliseconds();
const resMerge = mezcla(arrM);
const endTime = moment().milliseconds();
document.getElementById("p4").innerHTML = `Merge Sort: ${endTime-startTime} milesegundos`;
console.log(resMerge);
resMerge;

// MÉTODO RÁPIDO
const rapido = ([x = [], ...xs]) => {
  return x.length === 0
    ? []
    : [
        ...rapido(xs.filter((y) => y <= x)),
        x,
        ...rapido(xs.filter((y) => y > x)),
      ];
};

const arrQ = arrB;
const startTimeQ = moment().milliseconds();
const resQuick = rapido(arrQ);
const endTimeQ = moment().milliseconds();
document.getElementById("p5").innerHTML = `Quick sort: ${endTimeQ-startTimeQ} milisegundos`;
console.log(resQuick);
resQuick;

// MÉTODO DE CONTEO
const conteo = (inputArr, n = inputArr.length) => {
  const startTime = moment().milliseconds();
  let endTime = null;
  let k = Math.max(...inputArr);
  let t;
  //Crear un temporal con 0 valor cero
  //como la misma longitud de max elemet + 1
  const temp = new Array(k + 1).fill(0);
  //Cuente la frecuencia de cada elemento en la matriz original
  //Y guárdelo en una matriz temporal
  for (let i = 0; i < n; i++) {
    t = inputArr[i];
    temp[t]++;
  }
  //Actualizar el conteo basado en el índice anterior
  for (let i = 1; i <= k; i++) {
    // Actualización de elementos de la matriz de conteo
    temp[i] = temp[i] + temp[i - 1];
  }
  //Salida arr
  const outputArr = new Array(n).fill(0);

  for (let i = n - 1; i >= 0; i--) {
    // Agregar elementos de la matriz A a la matriz B
    t = inputArr[i];
    outputArr[temp[t] - 1] = t;
    // Disminuir el valor de conteo en 1
    temp[t] = temp[t] - 1;
  }

  // return outputArr;
  endTime = moment().milliseconds();
  return {
    data: outputArr,
    time: (endTime-startTime)
  }
};

const arrC = arrB;
const resCounting = conteo(arrC);
document.getElementById("p6").innerHTML = `Counting sort: ${resCounting.time} milisegundos`
console.log(resCounting);
resCounting;

// Obtener una referencia al elemento canvas del DOM
const $grafica = document.querySelector("#grafica");
// Las etiquetas son las que van en el eje X.
const etiquetas = ["# 1","# 2","# 3","# 4","# 5","# 6","# 7","# 8","# 9","# 10","# 11","# 12","# 13","# 14","# 15","# 16","# 17","# 18","# 19","# 20","# 21","# 22","# 23","# 24","# 25","# 26","# 27","# 28","# 29","# 30","# 31","# 32","# 33","# 34","# 35","# 36","# 37","# 38","# 39","# 40","# 41","# 42","# 43","# 44","# 45","# 46","# 47","# 48","# 49","# 50"
];
// Podemos tener varios conjuntos de datos. Comencemos con uno
const bubbleSort = {
  label: "Bubble sort",
  data: resBubble, // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas
  backgroundColor: "rgba(54, 162, 235, 0.2)", // Color de fondo
  borderColor: "rgba(54, 162, 235, 1)", // Color del borde
  borderWidth: 1, // Ancho del borde
};

const insertionSort = {
  label: "Insertion sort",
  data: resInsertion,
  backgroundColor: "rgba(255, 159, 64, 0.2)",
  borderColor: "rgba(255, 159, 64, 1)",
  borderWidth: 1,
};

const selectionSort = {
  label: "Selection sort",
  data: resSelection,
  backgroundColor: "rgba(149, 10, 13, 0.2)",
  borderColor: "rgba(149, 10, 13, 1)",
  borderWidth: 1,
};

const mergeSort = {
  label: "Merge sort",
  data: resMerge,
  backgroundColor: "rgba(248, 241, 18, 0.2)",
  borderColor: "rgba(248, 241, 18, 1)",
  borderWidth: 1,
};

const quickSort = {
  label: "Quick sort",
  data: resQuick,
  backgroundColor: "rgba(245, 0, 135, 0.2)",
  borderColor: "rgba(245, 0, 135, 1)",
  borderWidth: 1,
};

const countingSort = {
  label: "Counting sort",
  data: resCounting,
  backgroundColor: "rgba(18, 132, 69, 0.2)",
  borderColor: "rgba(18, 132, 69, 1)",
  borderWidth: 1,
};

new Chart($grafica, {
  type: "line",
  data: {
    labels: etiquetas,
    datasets: [
      bubbleSort,
      insertionSort,
      selectionSort,
      mergeSort,
      quickSort,
      countingSort,
    ],
  },
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: false,
          },
        },
      ],
    },
  },
});
